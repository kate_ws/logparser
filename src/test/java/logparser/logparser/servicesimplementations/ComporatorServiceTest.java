package logparser.logparser.servicesimplementations;

import logparser.logparser.models.dto.request.ComporationDto;
import logparser.logparser.models.dto.response.LogsFormatDto;
import logparser.logparser.models.enums.LogsComporation;
import logparser.logparser.services.impl.LogsFormatDtoComporator;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class ComporatorServiceTest {

    @Test
    public void testComporator() {
        ComporationDto comporationDto = new ComporationDto();
        comporationDto.setComparableList(createFakeList());
        comporationDto.setLogsComporation(LogsComporation.MESSAGE);

        List<LogsFormatDto> processingList = comporationDto.getComparableList();
        processingList.sort(new LogsFormatDtoComporator(comporationDto.getLogsComporation()));
        Assert.assertNotEquals(createFakeList(), processingList);
    }

    private List<LogsFormatDto> createFakeList() {
        List<LogsFormatDto> logsFormatDtos = new ArrayList<>();
        LogsFormatDto logA = new LogsFormatDto();
        logA.setTimestamp("27/Oct/2000:09:27:09 -0400");
        logA.setType("ERROR");
        logA.setMassage("A");
        logsFormatDtos.add(logA);
        LogsFormatDto logC = new LogsFormatDto();
        logC.setTimestamp("27/Oct/2000:09:37:09 -0400");
        logC.setType("ERROR");
        logC.setMassage("C");
        logsFormatDtos.add(logC);
        LogsFormatDto logB = new LogsFormatDto();
        logB.setTimestamp("27/Oct/2010:09:27:19 -0400");
        logB.setType("ERROR");
        logB.setMassage("B");
        logsFormatDtos.add(logB);
        LogsFormatDto logC2 = new LogsFormatDto();
        logC2.setTimestamp("27/Oct/2000:09:37:09 -0400");
        logC2.setType("ERROR");
        logC2.setMassage("C");
        logsFormatDtos.add(logC2);
        LogsFormatDto logC3 = new LogsFormatDto();
        logC3.setTimestamp("27/Oct/2000:09:37:09 -0400");
        logC3.setType("ERROR");
        logC3.setMassage("C");
        logsFormatDtos.add(logC);
        return logsFormatDtos;
    }
}
