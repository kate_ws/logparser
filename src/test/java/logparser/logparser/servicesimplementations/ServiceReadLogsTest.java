package logparser.logparser.servicesimplementations;

import logparser.logparser.models.dto.request.ComporationDto;
import logparser.logparser.models.dto.response.LogsFormatDto;
import logparser.logparser.services.api.ComporatorService;
import logparser.logparser.services.impl.ReadLogsServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ServiceReadLogsTest {

    @InjectMocks
    private ReadLogsServiceImpl readLogsService;

    @Mock
    private ComporatorService comporatorService;

    @BeforeEach
    void setUp() {
        readLogsService.setReadPath("./logs");
        readLogsService.setWritePath("./statistics");

        List<LogsFormatDto> logsFormatDtos = readLogsService.getTestLists();

        when(comporatorService.initCompare(any(ComporationDto.class))).thenReturn(logsFormatDtos);
    }


    /**
     * response will be unsorted
     */
    @Test
    void readLogsTest() {
        readLogsService.reportToFile();
    }
}
