package logparser.logparser.models.enums;

public enum LogsComporation {
    TIMESTAMP, TYPE, MESSAGE
}
