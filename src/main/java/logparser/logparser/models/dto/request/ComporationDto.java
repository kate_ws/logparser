package logparser.logparser.models.dto.request;

import logparser.logparser.models.dto.response.LogsFormatDto;
import logparser.logparser.models.enums.LogsComporation;

import java.util.List;

public class ComporationDto {

    private List<LogsFormatDto> comparableList;
    private LogsComporation logsComporation;

    public List<LogsFormatDto> getComparableList() {
        return this.comparableList;
    }
    public LogsComporation getLogsComporation() {
        return this.logsComporation;
    }
    public void setComparableList(List<LogsFormatDto> comparableList) {
        this.comparableList = comparableList;
    }
    public void setLogsComporation(LogsComporation logsComporation) {
        this.logsComporation = logsComporation;
    }
}
