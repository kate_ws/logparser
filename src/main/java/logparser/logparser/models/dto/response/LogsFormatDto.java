package logparser.logparser.models.dto.response;

import java.io.Serializable;

public class LogsFormatDto implements Serializable {

    private static final long serialVersionUID = 8472859907058016565L;

    private String timestamp;
    private String type;
    private String massage;

    public String getTimestamp() {
        return this.timestamp;
    }
    public String getType() {
        return this.type;
    }
    public String getMassage() {
        return this.massage;
    }
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
    public void setType(String type) {
        this.type = type;
    }
    public void setMassage(String massage) {
        this.massage = massage;
    }
}
