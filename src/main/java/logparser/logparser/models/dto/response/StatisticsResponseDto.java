package logparser.logparser.models.dto.response;

import java.util.List;

public class StatisticsResponseDto {

    private List<LogsFormatDto> logsFormatDtos;
    private Long totalElements;


    public List<LogsFormatDto> getLogsFormatDtos() {
        return this.logsFormatDtos;
    }
    public Long getTotalElements() {
        return this.totalElements;
    }
    public void setLogsFormatDtos(List<LogsFormatDto> logsFormatDtos) {
        this.logsFormatDtos = logsFormatDtos;
    }
    public void setTotalElements(Long totalElements) {
        this.totalElements = totalElements;
    }
}
