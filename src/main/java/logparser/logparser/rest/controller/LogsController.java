package logparser.logparser.rest.controller;

import logparser.logparser.models.dto.response.RestResponseEntity;
import logparser.logparser.services.api.ReadLogsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/logs", produces = MediaType.APPLICATION_JSON_VALUE)
public class LogsController {

    private ReadLogsService logsService;

    @Autowired
    public LogsController(ReadLogsService logsService) {
        this.logsService = logsService;
    }

    @GetMapping(value = "/get-to-rest")
    public RestResponseEntity getToRest() {
        return logsService.reportToRest();
    }

    @GetMapping(value = "/get-to-file")
    public RestResponseEntity getToFile() {
        return logsService.reportToFile();
    }

}
