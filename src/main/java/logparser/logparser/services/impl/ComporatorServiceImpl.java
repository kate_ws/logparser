package logparser.logparser.services.impl;

import logparser.logparser.models.dto.request.ComporationDto;
import logparser.logparser.models.dto.response.LogsFormatDto;
import logparser.logparser.services.api.ComporatorService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ComporatorServiceImpl implements ComporatorService {

    @Override
    public List<LogsFormatDto> initCompare(ComporationDto comporationDto) {
        List<LogsFormatDto> processingList = comporationDto.getComparableList();
        processingList.sort(new LogsFormatDtoComporator(comporationDto.getLogsComporation()));
        return processingList;
    }
}
