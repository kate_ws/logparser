package logparser.logparser.services.impl;

import com.google.gson.Gson;
import logparser.logparser.models.dto.request.ComporationDto;
import logparser.logparser.models.dto.response.LogsFormatDto;
import logparser.logparser.models.dto.response.RestResponseEntity;
import logparser.logparser.models.dto.response.StatisticsResponseDto;
import logparser.logparser.models.enums.LogsComporation;
import logparser.logparser.services.api.ComporatorService;
import logparser.logparser.services.api.ReadLogsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

@Service
public class ReadLogsServiceImpl implements ReadLogsService {

    @Value("${logger.read-path}")
    private String readPath;

    @Value("${logger.write-path}")
    private String writePath;

    private ComporatorService comporatorService;

    public void setReadPath(String readPath) {
        this.readPath = readPath;
    }
    public void setWritePath(String writePath) {
        this.writePath = writePath;
    }

    @Autowired
    public ReadLogsServiceImpl(ComporatorService comporatorService) {
        this.comporatorService = comporatorService;
    }


    @Override
    public RestResponseEntity reportToFile() {
        Gson gson = new Gson();
        localUpdateFile(gson.toJson(genResponseElement()));
        return new RestResponseEntity(String.format("report saved to directory %s", writePath));
    }

    @Override
    public RestResponseEntity reportToRest() {
        return new RestResponseEntity(genResponseElement());
    }

    private StatisticsResponseDto genResponseElement() {
        StatisticsResponseDto statisticsResponseDto = new StatisticsResponseDto();
        List<LogsFormatDto> responseList = getLists();
        statisticsResponseDto.setLogsFormatDtos(responseList);
        statisticsResponseDto.setTotalElements((long) Optional.ofNullable(responseList.size()).orElse(0));
        return statisticsResponseDto;
    }

    private List<LogsFormatDto> getLists() {
        File folder = new File(readPath);
        List<File> listOfFiles = Arrays.asList(Optional.ofNullable(folder.listFiles())
                .orElse(new File[0]));

        List<LogsFormatDto> responseObjects = new ArrayList<>();

        for (File file : listOfFiles) {
            List<String> lines = Collections.emptyList();
            try {
                lines = Files.readAllLines(Paths.get(file.getAbsolutePath()), StandardCharsets.UTF_8);
            } catch (IOException e) {
                e.printStackTrace();
            }
            responseObjects.addAll(dataFormatter(lines));
        }

        responseObjects = selectOnlyErrors(responseObjects);


        /*
         * actually there could call comporation to another field. It will be easy to modify service, to make
         * comporation parameter changable
         */
        ComporationDto comporationDto = new ComporationDto();
        comporationDto.setComparableList(responseObjects);
        comporationDto.setLogsComporation(LogsComporation.MESSAGE);

        responseObjects = comporatorService.initCompare(comporationDto);
        return responseObjects;
    }

    private List<LogsFormatDto> dataFormatter(List<String> inputStringLists) {
        List<LogsFormatDto> listFormats = new ArrayList<>();
        for (String inputString : inputStringLists) {
            LogsFormatDto dto = new LogsFormatDto();
            StringTokenizer matcher = new StringTokenizer(inputString);
            dto.setTimestamp(matcher.nextToken("]").substring(1));
            dto.setType(matcher.nextToken("]").substring(2));
            String masage = matcher.nextToken();
            dto.setMassage(masage.substring(2, masage.length()-1));
            listFormats.add(dto);
        }
        return listFormats;
    }

    private List<LogsFormatDto> selectOnlyErrors(List<LogsFormatDto> inputDtoList) {
        List<LogsFormatDto> responseList = new ArrayList<>();
        for (LogsFormatDto inputDtoElement : inputDtoList) {
            if ("ERROR".equals(inputDtoElement.getType())) {
                responseList.add(inputDtoElement);
            }
        }
        return responseList;
    }

    private void localUpdateFile(final String objects) {
        try (FileWriter file = new FileWriter(writePath+"/Statistics.json")) {
            file.write(objects);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }


    /**
     * method is for testing only
     */
    public List<LogsFormatDto> getTestLists() {
        File folder = new File(readPath);
        List<File> listOfFiles = Arrays.asList(Optional.ofNullable(folder.listFiles())
                .orElse(new File[0]));

        List<LogsFormatDto> responseObjects = new ArrayList<>();

        for (File file : listOfFiles) {
            List<String> lines = Collections.emptyList();
            try {
                lines = Files.readAllLines(Paths.get(file.getAbsolutePath()), StandardCharsets.UTF_8);
            } catch (IOException e) {
                e.printStackTrace();
            }
            responseObjects.addAll(dataFormatter(lines));
        }

        responseObjects = selectOnlyErrors(responseObjects);

        return responseObjects;
    }

}
