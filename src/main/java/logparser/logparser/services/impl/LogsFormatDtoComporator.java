package logparser.logparser.services.impl;

import com.google.common.collect.ImmutableMap;
import logparser.logparser.models.dto.response.LogsFormatDto;
import logparser.logparser.models.enums.LogsComporation;

import java.util.Comparator;
import java.util.Map;

public class LogsFormatDtoComporator implements Comparator<LogsFormatDto> {

    private LogsComporation requestSortField;

    private static final Comparator<String> nullSafeStringComparator = Comparator
            .nullsFirst(String::compareToIgnoreCase);

    private static final Comparator<LogsFormatDto> massageComporator = Comparator
            .comparing(LogsFormatDto::getMassage, nullSafeStringComparator)
            .thenComparing(LogsFormatDto::getTimestamp, nullSafeStringComparator)
            .thenComparing(LogsFormatDto::getType, nullSafeStringComparator);

    private static final Comparator<LogsFormatDto> timestampComporator = Comparator
            .comparing(LogsFormatDto::getTimestamp, nullSafeStringComparator)
            .thenComparing(LogsFormatDto::getMassage, nullSafeStringComparator)
            .thenComparing(LogsFormatDto::getType, nullSafeStringComparator);

    private static final Comparator<LogsFormatDto> typeComporator = Comparator
            .comparing(LogsFormatDto::getType, nullSafeStringComparator)
            .thenComparing(LogsFormatDto::getTimestamp, nullSafeStringComparator)
            .thenComparing(LogsFormatDto::getMassage, nullSafeStringComparator);


    private static final Map<LogsComporation, Comparator<LogsFormatDto>> dataProcessingMap
            = ImmutableMap.<LogsComporation, Comparator<LogsFormatDto>>builder()
            .put(LogsComporation.MESSAGE, massageComporator)
            .put(LogsComporation.TIMESTAMP, timestampComporator)
            .put(LogsComporation.TYPE, typeComporator)
            .build();


    public int compare(LogsFormatDto logsFormatDto1,
                       LogsFormatDto logsFormatDto2) {
        return dataProcessingMap.get(requestSortField).compare(logsFormatDto1, logsFormatDto2);
    }

    public LogsFormatDtoComporator(LogsComporation requestSortField) {
        this.requestSortField = requestSortField;
    }
}
