package logparser.logparser.services.api;

import logparser.logparser.models.dto.response.RestResponseEntity;

/**
 * This service implements log-parser calls
 */

public interface ReadLogsService {

    /**
     * sends report of errors to file
     */
    RestResponseEntity reportToFile();

    /**
     * sends report of errors to RestResponseEntity data element
     */
    RestResponseEntity reportToRest();

}
