package logparser.logparser.services.api;

import logparser.logparser.models.dto.request.ComporationDto;
import logparser.logparser.models.dto.response.LogsFormatDto;

import java.util.List;

/**
 * This service implements comporation for lists of log data elements
 */
public interface ComporatorService {
    /**
     * Compare elements in lists, returns resorted lists
     *
     * @param comporationDto    object described comporation field and List of comporated objects
     */
    List<LogsFormatDto> initCompare(ComporationDto comporationDto);
}
